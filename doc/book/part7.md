# Part 7: Make the album module more modular

The previous parts are more or less directly taken over from
[Ralf Eggert's zend-expressive-tutorial](https://github.com/ralfeggert/zend-expressive-tutorial).

The following parts are not as detailed as the previous ones.
They will cover some additional topics, that have not been covered yet.
Most of the input for these parts comes from different sources from all over the web.
The sources are linked as far as they are known.

Let's start with some more modularization of the `album module`

The album module requires some configuration, which is found in two different places:

* factories:    `config/autoload/album.global.php`
* templates:    `config/autoload/album.global.php`
* routing:      `config/routes.php`

A re-usable module should contain all relevant configuration in one single path -
the module path `/src/Album`.

## Add configuration with `ConfigProvider.php`

A `ConfigProvider.php` file provides the possibility to add configuration for a module in
a single file. A lot of `Zend` packages make use of `ConfigProvider.php` files as
we have seen in previous parts of this book.
As we have used the `expressive module` script in
[part2_album-module](https://gitlab.com/lowtower/zend-expressive2-tutorial/tree/part2_album-module) of the series, the
`ConfigProvider.php` file already exists in the path `/src/Album/src/`.
It holds all the configuration from `config/autoload/album.global.php`, which can
be deleted afterwards.

```php
<?php
declare(strict_types = 1);

namespace Album;

/**
 * The configuration provider for the Album module
 *
 * @package Album
 * @see https://docs.zendframework.com/zend-component-installer
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     * @return array
     */
    public function __invoke()
    {
        return [
            'dependencies' => $this->getDependencies(),
            'templates'    => $this->getTemplates(),
        ];
    }

    /**
     * Returns the container dependencies
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            'factories' => [
            Action\AlbumListAction::class => Action\AlbumListActionFactory::class,

            Action\AlbumCreateFormAction::class   =>
                Action\AlbumCreateFormActionFactory::class,
            Action\AlbumCreateHandleAction::class =>
                Action\AlbumCreateHandleActionFactory::class,
            Action\AlbumUpdateFormAction::class =>
                Action\AlbumUpdateFormActionFactory::class,
            Action\AlbumUpdateHandleAction::class =>
                Action\AlbumUpdateHandleActionFactory::class,
            Action\AlbumDeleteFormAction::class =>
                Action\AlbumDeleteFormActionFactory::class,
            Action\AlbumDeleteHandleAction::class =>
                Action\AlbumDeleteHandleActionFactory::class,

            Model\Repository\AlbumRepositoryInterface::class =>
                Model\Repository\AlbumRepositoryFactory::class,

            Model\Storage\AlbumStorageInterface::class =>
                Db\AlbumTableGatewayFactory::class,

            Form\AlbumDataForm::class =>
                Form\AlbumDataFormFactory::class,
            Form\AlbumDeleteForm::class =>
                Form\AlbumDeleteFormFactory::class,

            Model\InputFilter\AlbumInputFilter::class =>
                Model\InputFilter\AlbumInputFilterFactory::class,
            ],
        ];
    }

    /**
     * Returns the templates configuration
     *
     * @return array
     */
    public function getTemplates()
    {
        return [
            'paths' => [
                'album' => [__DIR__ . '/../templates/album'],
            ],
        ];
    }
}
```

```bash
$ rm config/autoload/album.global.php
```

or use whatever tools you are comfortable with.

## Add Routing with a DelegatorFactory

[In the expressive cookbook](https://docs.zendframework.com/zend-expressive/cookbook/autowiring-routes-and-pipelines)
a delegator factory is proposed as a means of "autowiring" routes into a project.
Let's use it here.

Create a file `RoutesDelegator.php` in the path `/src/Album/src/Factory/` with all
the "album" routes from `config/routes.php`, which have to be removed from that
file afterwards.
But, we will keep the `home` route in that file as it is our global entry point to our website.

```php
<?php
declare(strict_types = 1);

namespace Album\Factory;

use Psr\Container\ContainerInterface;
use Zend\Expressive\Application;
use Album\Action;

class RoutesDelegator
{
    /**
     * @param ContainerInterface $container
     * @param string $serviceName Name of the service being created.
     * @param callable $callback Creates and returns the service.
     * @return Application
     */
    public function __invoke(ContainerInterface $container, $serviceName, callable $callback)
    {
        /** @var $app Application */
        $app = $callback();

        // Setup routes:
        $app->get('/album', Action\AlbumListAction::class, 'album');
        $app->get('/album/create', Action\AlbumCreateFormAction::class, 'album-create');
        $app->post('/album/create/handle',[
            Action\AlbumCreateHandleAction::class,
            Action\AlbumCreateFormAction::class,
        ], 'album-create-handle');
        $app->get("/album/update/{id:\d+}", Action\AlbumUpdateFormAction::class, 'album-update');
        $app->post("/album/update/{id:\d+}/handle", [
            Action\AlbumUpdateHandleAction::class,
            Action\AlbumUpdateFormAction::class,
        ], 'album-update-handle');
        $app->get("/album/delete/{id:\d+}", Action\AlbumDeleteFormAction::class, 'album-delete');
        $app->post("/album/delete/{id:\d+}/handle", [
            Action\AlbumDeleteHandleAction::class,
            Action\AlbumDeleteFormAction::class,
        ], 'album-delete-handle');

        return $app;
    }
}
```

Once you've created this, edit the class `Album\ConfigProvider`; in it, we'll update
the `getDependencies()` method to add the delegator factory:

```php
<?php
public function getDependencies()
{
    return [
        /* . . . */
        'delegators' => [
            \Zend\Expressive\Application::class => [
                Factory\RoutesDelegator::class,
            ],
        ],
    ];
}
```

## Finish

Now you are done with your first PSR-7 middleware. You have created a
lightweight application to handle albums with `Zend\Expressive`. Please
have a closer look at the generated code now and try to understand
everything you have done in the six parts of this tutorial.

## Compare with example repository branch `part7_modular`

You can easily compare your code with the example repository when looking
at the branch `part7_modular`. If you want you can even clone it and have a deeper
look.

[https://gitlab.com/lowtower/zend-expressive2-tutorial/tree/part7_modular](https://gitlab.com/lowtower/zend-expressive2-tutorial/tree/part7_modular)
