# Using zend-paginator in your Album Module

In this tutorial, we will use the [zend-paginator component](https://zendframework.github.io/zend-paginator)
to add a handy pagination controller to the bottom of the album list.

Currently, we only have a handful of albums to display, so showing everything on
one page is not a problem. However, how will the album list look when we have
100 albums or more in our database? The standard solution to this problem is to
split the data up into a number of pages, and allow the user to navigate around
these pages using a pagination control. Just type "Zend Framework" into Google,
and you can see their pagination control at the bottom of the page:

![Example pagination control](images/pagination.sample.png)

## Preparation

As before, we are going to use sqlite, via PHP's PDO driver.
To add some more albums to our database, perform `SQL` commands listed below:

```sql
INSERT INTO `album` (`artist`, `title`)
    VALUES ('David Bowie', 'The Next Day (Deluxe Version)');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Bastille', 'Bad Blood');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Bruno Mars', 'Unorthodox Jukebox');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Emeli Sandé', 'Our Version of Events (Special Edition)');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Bon Jovi', 'What About Now (Deluxe Version)');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Justin Timberlake', 'The 20/20 Experience (Deluxe Version)');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Bastille', 'Bad Blood (The Extended Cut)');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('P!nk', 'The Truth About Love');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Sound City - Real to Reel', 'Sound City - Real to Reel');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Jake Bugg', 'Jake Bugg');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Various Artists', 'The Trevor Nelson Collection');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('David Bowie', 'The Next Day');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Mumford & Sons', 'Babel');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('The Lumineers', 'The Lumineers');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Various Artists', 'Get Ur Freak On - R&B Anthems');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('The 1975', 'Music For Cars EP');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Various Artists', 'Saturday Night Club Classics - Ministry of Sound');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Hurts', 'Exile (Deluxe)');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Various Artists', 'Mixmag - The Greatest Dance Tracks of All Time');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Ben Howard', 'Every Kingdom');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Stereophonics', 'Graffiti On the Train');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('The Script', '#3');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Stornoway', 'Tales from Terra Firma');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('David Bowie', 'Hunky Dory (Remastered)');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Worship Central', 'Let It Be Known (Live)');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Ellie Goulding', 'Halcyon');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Various Artists', 'Dermot O''Leary Presents the Saturday Sessions 2013');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Stereophonics', 'Graffiti On the Train (Deluxe Version)');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Dido', 'Girl Who Got Away (Deluxe)');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Hurts', 'Exile');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Bruno Mars', 'Doo-Wops & Hooligans');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Calvin Harris', '18 Months');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Olly Murs', 'Right Place Right Time');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Alt-J (?)', 'An Awesome Wave');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('One Direction', 'Take Me Home');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Various Artists', 'Pop Stars');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Various Artists', 'Now That''s What I Call Music! 83');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('John Grant', 'Pale Green Ghosts');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Paloma Faith', 'Fall to Grace');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Laura Mvula', 'Sing To the Moon (Deluxe)');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Duke Dumont', 'Need U (100%) [feat. A*M*E] - EP');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Watsky', 'Cardboard Castles');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Blondie', 'Blondie: Greatest Hits');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Foals', 'Holy Fire');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Maroon 5', 'Overexposed');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Bastille', 'Pompeii (Remixes) - EP');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Imagine Dragons', 'Hear Me - EP');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Various Artists', '100 Hits: 80s Classics');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Various Artists', 'Les Misérables (Highlights From the Motion Picture Soundtrack)');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Mumford & Sons', 'Sigh No More');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Frank Ocean', 'Channel ORANGE');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Bon Jovi', 'What About Now');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Various Artists', 'BRIT Awards 2013');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Taylor Swift', 'Red');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Fleetwood Mac', 'Fleetwood Mac: Greatest Hits');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('David Guetta', 'Nothing But the Beat Ultimate');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Various Artists', 'Clubbers Guide 2013 (Mixed By Danny Howard) - Ministry of Sound');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('David Bowie', 'Best of Bowie');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Laura Mvula', 'Sing To the Moon');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('ADELE', '21');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Of Monsters and Men', 'My Head Is an Animal');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Rihanna', 'Unapologetic');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Various Artists', 'BBC Radio 1''s Live Lounge - 2012');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Avicii & Nicky Romero', 'I Could Be the One (Avicii vs. Nicky Romero)');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('The Streets', 'A Grand Don''t Come for Free');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Tim McGraw', 'Two Lanes of Freedom');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Foo Fighters', 'Foo Fighters: Greatest Hits');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Various Artists', 'Now That''s What I Call Running!');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Swedish House Mafia', 'Until Now');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('The xx', 'Coexist');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Five', 'Five: Greatest Hits');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Jimi Hendrix', 'People, Hell & Angels');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Biffy Clyro', 'Opposites (Deluxe)');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('The Smiths', 'The Sound of the Smiths');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('The Saturdays', 'What About Us - EP');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Fleetwood Mac', 'Rumours');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Various Artists', 'The Big Reunion');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Various Artists', 'Anthems 90s - Ministry of Sound');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('The Vaccines', 'Come of Age');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Nicole Scherzinger', 'Boomerang (Remixes) - EP');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Bob Marley', 'Legend (Bonus Track Version)');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Josh Groban', 'All That Echoes');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Blue', 'Best of Blue');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Ed Sheeran', '+');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Olly Murs', 'In Case You Didn''t Know (Deluxe Edition)');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Macklemore & Ryan Lewis', 'The Heist (Deluxe Edition)');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Various Artists', 'Defected Presents Most Rated Miami 2013');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Gorgon City', 'Real EP');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Mumford & Sons', 'Babel (Deluxe Version)');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Various Artists', 'The Music of Nashville: Season 1, Vol. 1 (Original Soundtrack)');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Various Artists', 'The Twilight Saga: Breaking Dawn, Pt. 2 (Original Motion Picture Soundtrack)');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Various Artists', 'Mum - The Ultimate Mothers Day Collection');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('One Direction', 'Up All Night');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Bon Jovi', 'Bon Jovi Greatest Hits');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Agnetha Fältskog', 'A');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Fun.', 'Some Nights');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Justin Bieber', 'Believe Acoustic');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Atoms for Peace', 'Amok');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Justin Timberlake', 'Justified');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Passenger', 'All the Little Lights');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Kodaline', 'The High Hopes EP');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Lana Del Rey', 'Born to Die');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('JAY Z & Kanye West', 'Watch the Throne (Deluxe Version)');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Biffy Clyro', 'Opposites');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Various Artists', 'Return of the 90s');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Gabrielle Aplin', 'Please Don''t Say You Love Me - EP');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Various Artists', '100 Hits - Driving Rock');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Jimi Hendrix', 'Experience Hendrix - The Best of Jimi Hendrix');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Various Artists', 'The Workout Mix 2013');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('The 1975', 'Sex');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Chase & Status', 'No More Idols');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Rihanna', 'Unapologetic (Deluxe Version)');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('The Killers', 'Battle Born');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Olly Murs', 'Right Place Right Time (Deluxe Edition)');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('A$AP Rocky', 'LONG.LIVE.A$AP (Deluxe Version)');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Various Artists', 'Cooking Songs');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Haim', 'Forever - EP');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Lianne La Havas', 'Is Your Love Big Enough?');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Michael Bublé', 'To Be Loved');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Daughter', 'If You Leave');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('The xx', 'xx');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Eminem', 'Curtain Call');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Kendrick Lamar', 'good kid, m.A.A.d city (Deluxe)');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Disclosure', 'The Face - EP');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Palma Violets', '180');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Cody Simpson', 'Paradise');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Ed Sheeran', '+ (Deluxe Version)');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Michael Bublé', 'Crazy Love (Hollywood Edition)');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Bon Jovi', 'Bon Jovi Greatest Hits - The Ultimate Collection');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Rita Ora', 'Ora');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('g33k', 'Spabby');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Various Artists', 'Annie Mac Presents 2012');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('David Bowie', 'The Platinum Collection');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Bridgit Mendler', 'Ready or Not (Remixes) - EP');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Dido', 'Girl Who Got Away');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Various Artists', 'Now That''s What I Call Disney');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('The 1975', 'Facedown - EP');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Kodaline', 'The Kodaline - EP');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Various Artists', '100 Hits: Super 70s');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Fred V & Grafix', 'Goggles - EP');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Biffy Clyro', 'Only Revolutions (Deluxe Version)');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Train', 'California 37');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Ben Howard', 'Every Kingdom (Deluxe Edition)');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Various Artists', 'Motown Anthems');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Courteeners', 'ANNA');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Johnny Marr', 'The Messenger');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Rodriguez', 'Searching for Sugar Man');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Jessie Ware', 'Devotion');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Bruno Mars', 'Unorthodox Jukebox');
INSERT INTO `album` (`artist`, `title`)
    VALUES ('Various Artists', 'Call the Midwife (Music From the TV Series)');
```

## Install zend-paginator

`zend-paginator` is not installed or configured by default, so we will need to do
that. Run the following from the application root:

```bash
$ composer require zendframework/zend-paginator
```
![Install Zend\Paginator component](images/install-zend-paginator.png)

Once installed, our application is now aware of zend-paginator, and even has
some default factories in place, which we will now make use of.

## Modifying the AlbumTable

In order to let zend-paginator handle our database queries automatically for us,
we will be using the [DbSelect pagination adapter](https://zendframework.github.io/zend-paginator/usage/#the-dbselect-adapter)
This will automatically manipulate and run a `Zend\Db\Sql\Select` object to
include the correct `LIMIT` and `WHERE` clauses so that it returns only the
configured amount of data for the given page. Let's modify the `fetchAll` method
of the `AlbumTable` model, so that it can optionally return a paginator object:

```php
<?php
// in src/Album/src/Model/Repository/AlbumRepository.php:
namespace Album\Model\Repository;

use RuntimeException;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class AlbumTable
{
    /* ... */

    public function fetchAlbumList($paginated = false)
    {
        if ($paginated) {
            return $this->fetchPaginatedResults();
        }

        $select = $this->getSql()->select();

        $collection = [];

        /** @var AlbumEntity $entity */
        foreach ($this->selectWith($select) as $entity) {
            $collection[$entity->getId()] = $entity;
        }

        return $collection;
    }

    private function fetchPaginatedResults()
    {
        // Create a new Select object for the table:
        $select = new Select($this->getTable());

        // Create a new result set based on the Album entity:
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new AlbumEntity());

        // Create a new pagination adapter object:
        $paginatorAdapter = new DbSelect(
            // our configured select object:
            $select,
            // the adapter to run it against:
            $this->getAdapter(),
            // the result set to hydrate:
            $resultSetPrototype
        );

        $paginator = new Paginator($paginatorAdapter);
        return $paginator;
    }

    /* ... */
}
```

This will return a fully configured `Paginator` instance. We've already told the
`DbSelect` adapter to use our created `Select` object, to use the adapter that
the `TableGateway` object uses, and also how to hydrate the result into a
`Album` entity in the same fashion as the `TableGateway` does. This means that
our executed and returned paginator results will return `Album` objects in
exactly the same fashion as the non-paginated results.

## Modifying the AlbumListAction

Next, we need to tell the album list action to provide the view with a
`Pagination` object instead of a `ResultSet`. Both these objects can by iterated
over to return hydrated `Album` objects, so we won't need to make many changes
to the view script:


```php
<?php
// in src/Album/src/Action/AlbumListAction.php:

/* ... */

    public function process(ServerRequestInterface $request, DelegateInterface $delegate)
    {
        // Grab the paginator from the AlbumTable:
        $paginator = $this->albumRepository->fetchAllAlbums(true);

        // Set the current page to what has been passed in query string,
        // or to 1 if none is set, or the page is invalid:
        $page = 1;
        $uri  = $request->getUri();
        if (preg_match('#^page=([0-9]+)#', $uri->getQuery(), $matches)) {
            $page = $matches[1];
        }
        $page = ($page < 1) ? 1 : $page;
        $paginator->setCurrentPageNumber($page);
        // Set the number of items per page to 10:
        $paginator->setItemCountPerPage(10);

        $data = [
            'paginator' => $paginator,
        ];

        return new HtmlResponse(
            $this->template->render('album::list', $data)
        );
    }

/* ... */
```

Here we are getting the configured `Paginator` object from the `AlbumRepository`, and
then telling it to use the page that is optionally passed in the querystring
`page` parameter (after first validating it). We are also telling the paginator
we want to display 10 albums per page.

## Updating the View Script

Now, tell the view script to iterate over the `pagination` view variable, rather
than the `albums` variable:

```html
<?php
// in src/Album/templates/album/list.phtml:

use Album\Model\Entity\AlbumEntity;

$this->headTitle('Albums'); ?>

<div class="jumbotron">
    <h1>Album list</h1>
</div>

<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>Id</th>
        <th>Artist</th>
        <th>Title</th>
    </tr>
    </thead>
    <tbody>
    <?php /** @var AlbumEntity $albumEntity */ ?>
    <?php foreach ($this->paginator as $albumEntity) : ?>
        <?php
        $urlParams = ['id' => $albumEntity->getId()];
        $updateUrl = $this->url('album-update', $urlParams);
        $deleteUrl = $this->url('album-delete', $urlParams);
        ?>
        <tr>
            <td><?= $albumEntity->getId() ?></td>
            <td><?= $albumEntity->getArtist() ?></td>
            <td><?= $albumEntity->getTitle() ?></td>
            <td>
                <a href="<?=$updateUrl ?>" class="btn btn-success">
                    <i class="fa fa-pencil"></i>
                </a>
                <a href="<?=$deleteUrl ?>" class="btn btn-success">
                    <i class="fa fa-trash"></i>
                </a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
```

Checking the `/album` route on your website should now give you a list of just
10 albums, but with no method to navigate through the pages. Let's correct that
now.

## Creating the Pagination Control Partial

 We need to create a custom pagination control partial to render our pagination
control just the way we want it.
Again, because we are using Bootstrap, this will primarily involve
outputting correctly formatted HTML. Let's create the partial in the
`src/App/view/partial/` folder, so that we can use the control in all
our modules:

```html
<?php
// in src/App/view/partial/pagination.phtml:
?>

<?php if ($this->pageCount): ?>
<nav aria-label="Page navigation">
    <ul class="pagination">
    <!-- Previous page link -->
    <?php if (isset($this->previous)): ?>
        <li>
            <a href="<?= $this->url($this->route, [], ['page' => $this->previous]) ?>" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
            </a>
        </li>
    <?php else: ?>
        <li class="disabled">
            <a href="#" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
            </a>
        </li>
    <?php endif ?>

    <!-- Numbered page links -->
    <?php foreach ($this->pagesInRange as $page): ?>
        <?php if ($page !== $this->current): ?>
            <li>
                <a href="<?= $this->url($this->route, [], ['page' => $page]) ?>">
                    <?= $page ?>
                </a>
            </li>
        <?php else: ?>
            <li class="active">
                <a href="#"><?= $page ?></a>
            </li>
        <?php endif ?>
    <?php endforeach ?>

    <!-- Next page link -->
    <?php if (isset($this->next)): ?>
        <li>
            <a href="<?= $this->url($this->route, [], ['page' => $this->next]) ?>" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
            </a>
        </li>
    <?php else: ?>
        <li class="disabled" aria-label="Next">
            <a href="#">
                <span aria-hidden="true">&raquo;</span>
            </a>
        </li>
    <?php endif ?>
    </ul>
</nav>
<?php endif ?>
```

This partial creates a pagination control with links to the correct pages (if
there is more than one page in the pagination object). It will render a previous
page link (and mark it disabled if you are at the first page), then render a
list of intermediate pages (that are passed to the partial based on the
rendering style; we'll pass that to the view helper in the next step). Finally,
it will create a next page link (and disable it if you're at the end). Notice
how we pass the page number via the `page` querystring parameter which we have
already told our controller to use to display the current page.

### Using the PaginationControl View Helper

To page through the albums, we need to invoke the
[paginationControl view helper](https://zendframework.github.io/zend-paginator/usage/#rendering-pages-with-view-scripts)
to display our pagination control:

```php
<?php
// In src/Album/templates/album/list.phtml:
// Add at the end of the file after the table:
?>
<?= $this->paginationControl(
    // The paginator object:
    $this->paginator,
    // The scrolling style:
    'sliding',
    // The partial to use to render the control:
    'partial::pagination',
    // The route to link to when a user clicks a control link:
    ['route' => 'album']
) ?>
```

The above echoes the `paginationControl` helper, and tells it to use our
paginator instance, the [sliding scrolling style](https://zendframework.github.io/zend-paginator/advanced/#custom-scrolling-styles),
our paginator partial, and which route to use for generating links. Refreshing
your application now should give you Bootstrap-styled pagination controls!

## Compare with example repository branch `part9`

You can easily compare your code with the example repository when looking
at the branch `part9`. If you want you can even clone it and have a deeper
look.

[https://gitlab.com/lowtower/zend-expressive2-tutorial/tree/part9_pagination](https://gitlab.com/lowtower/zend-expressive2-tutorial/tree/part9_pagination)
