# Part 8: Config abstract factory

Every action needs its own factory class to inject all the depencies.
This is somewhat repetitve and cumbersome.
The zend-servicemanager v3.2 introduced a built-in factory that can inject
dependencies on services based on configuration.
Such a config abstract factory is described in [a blog post](https://blog.alejandrocelaya.com/2017/07/21/reusing-factories-in-zend-servicemanager) of Alejandro Celaya
makes action factories superfluous. For details have a look at the blog post.

## Check factories

Let's first have a look at all the factories we have:

1. `Album\Action\AlbumCreateFormActionFactory` only injects
    * `TemplateRendererInterface::class`
    * `AlbumDataForm::class`
 2. `Album\Action\AlbumCreateHandleActionFactory` only injects
    * `RouterInterface::class`
    * `AlbumRepositoryInterface::class`
    * `AlbumDataForm::class`
 3. `Album\Action\AlbumDeleteFormActionFactory` only injects
    * `TemplateRendererInterface::class`
    * `AlbumRepositoryInterface::class`
    * `AlbumDeleteForm::class`
 4. `Album\Action\AlbumDeleteHandleActionFactory` only injects
    * `RouterInterface::class`
    * `AlbumRepositoryInterface::class`
 5. `Album\Action\AlbumListActionFactory` only injects
    * `TemplateRendererInterface::class`
    * `AlbumRepositoryInterface::class`
 6. `Album\Action\AlbumUpdateFormActionFactory` only injects
    * `TemplateRendererInterface::class`
    * `AlbumRepositoryInterface::class`
    * `AlbumDataForm::class`
 7. `Album\Action\AlbumUpdateHandleActionFactory` only injects
    * `RouterInterface::class`
    * `AlbumRepositoryInterface::class`
    * `AlbumDataForm::class`
 8. `Album\Db\AlbumTableGatewayFactory`                 generates a new `Zend\Db\ResultSet\HydratingResultSet`.
 9. `Album\Form\AlbumDataFormFactory`                   retrieves an input filter, generates a form and sets hydrator and input filter to it.
10. `Album\Form\AlbumDeleteFormFactory`                 initialises and returns an `AlbumDeleteForm`.
11. `Album\Model\InputFilter\AlbumInputFilterFactory`   initialises and returns an `AlbumInputFilter`.
12. `Album\Model\Repository\AlbumRepositoryFactory`     injects `AlbumStorageInterface::class` to `Album\Model\Repository\AlbumRepositoryInterface`.

We have twelve factories, of which seven only grab some dependencies from the container and
create a new instance of an object where those dependencies are injected.
These are the action factories.
In these cases the `ConfigAbstractFactory` is perfect.

## Add configuration to `Album\ConfigProvider`

All the factories from above are in the same namespace `Album`.
The configuration of the `Album` module takes place in the file `/src/Album/src/ConfigProvider.php`.
So, let's change it to the following:

```php
<?php

declare(strict_types=1);

namespace Album;

use Zend\ServiceManager\AbstractFactory\ConfigAbstractFactory;

/**
 * The configuration provider for the Album module
 *
 * @see https://docs.zendframework.com/zend-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     * @return array
     */
    public function __invoke()
    {
        return [
            'dependencies'               => $this->getDependencies(),
            'templates'                  => $this->getTemplates(),
            ConfigAbstractFactory::class => $this->getConfigAbstractFactory(),
        ];
    }

    /**
     * Returns the container dependencies
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            'factories' => [
                Action\AlbumListAction::class         => ConfigAbstractFactory::class,
                Action\AlbumCreateFormAction::class   => ConfigAbstractFactory::class,
                Action\AlbumCreateHandleAction::class => ConfigAbstractFactory::class,
                Action\AlbumUpdateFormAction::class   => ConfigAbstractFactory::class,
                Action\AlbumUpdateHandleAction::class => ConfigAbstractFactory::class,
                Action\AlbumDeleteFormAction::class   => ConfigAbstractFactory::class,
                Action\AlbumDeleteHandleAction::class => ConfigAbstractFactory::class,

                Model\Repository\AlbumRepositoryInterface::class => Model\Repository\AlbumRepositoryFactory::class,

                Model\Storage\AlbumStorageInterface::class => Db\AlbumTableGatewayFactory::class,

                Form\AlbumDataForm::class   => Form\AlbumDataFormFactory::class,
                Form\AlbumDeleteForm::class => Form\AlbumDeleteFormFactory::class,

                Model\InputFilter\AlbumInputFilter::class => Model\InputFilter\AlbumInputFilterFactory::class,
            ],
            'delegators' => [
                \Zend\Expressive\Application::class => [
                    Factory\RoutesDelegator::class,
                ],
            ],
        ];
    }

    /**
     * Returns the container dependencies
     *
     * @return array
     */
    public function getConfigAbstractFactory()
    {
        return [
            Action\AlbumListAction::class => [
                \Zend\Expressive\Template\TemplateRendererInterface::class,
                Model\Repository\AlbumRepositoryInterface::class,
            ],
            Action\AlbumCreateFormAction::class => [
                \Zend\Expressive\Template\TemplateRendererInterface::class,
                Form\AlbumDataForm::class,
            ],
            Action\AlbumCreateHandleAction::class => [
                \Zend\Expressive\Router\RouterInterface::class,
                Model\Repository\AlbumRepositoryInterface::class,
                Form\AlbumDataForm::class,
            ],
            Action\AlbumDeleteFormAction::class => [
                \Zend\Expressive\Template\TemplateRendererInterface::class,
                Model\Repository\AlbumRepositoryInterface::class,
                Form\AlbumDeleteForm::class,
            ],
            Action\AlbumDeleteHandleAction::class => [
                \Zend\Expressive\Router\RouterInterface::class,
                Model\Repository\AlbumRepositoryInterface::class,
            ],
            Action\AlbumUpdateFormAction::class => [
                \Zend\Expressive\Template\TemplateRendererInterface::class,
                Model\Repository\AlbumRepositoryInterface::class,
                Form\AlbumDataForm::class,
            ],
            Action\AlbumUpdateHandleAction::class => [
                \Zend\Expressive\Router\RouterInterface::class,
                Model\Repository\AlbumRepositoryInterface::class,
                Form\AlbumDataForm::class,
            ],
        ];
    }

    /**
     * Returns the templates configuration
     *
     * @return array
     */
    public function getTemplates()
    {
        return [
            'paths' => [
                'album' => [__DIR__ . '/../templates/album'],
            ],
        ];
    }
}
```

We have added a new key to the config array, that contains the dependencies for the action classes,
which are now built with the `ConfigAbstractFactory` instead of the formerly used factories.

## Remove concrete factories

Now, we can remove all the action factory files that were defined in `src/Album/src/ConfigProvider.php`:

```bash
$ rm src/Album/src/Action/Album*ActionFactory.php
$ rm test/AlbumTest/Action/Album*ActionFactory.php
```
or use whatever tools you are comfortable with.

## Extract

Thanks to the `ConfigAbstractFactory` we were able to replace all the action factories with a single config abstract factory.

## Compare with example repository branch `part8`

You can easily compare your code with the example repository when looking
at the branch `part8`. If you want you can even clone it and have a deeper
look.

[https://gitlab.com/lowtower/zend-expressive2-tutorial/tree/part8_config_abstract_factory](https://gitlab.com/lowtower/zend-expressive2-tutorial/tree/part8_config_abstract_factory)
