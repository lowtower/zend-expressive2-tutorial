# Part 8: Reduce number of factories

Every action needs its own factory class to inject all the depencies.
This is somewhat repetitve and cumbersome.
There are several possible solutions to overcome this problem.

The first one, we will look at, is a reflection based abstract factory as described in
[a blog post] (https://xtreamwayz.com/blog/2015-12-30-psr7-abstract-action-factory-one-for-all) of xtreamwayz.
It makes action factories superfluous. For details have a look at the blog post.

Another approach is a config based approach using the `Zend\ServiceManager\AbstractFactory\ConfigAbstractFactory`,
which was introduced in the zend-servicemanager v3.2.
Such a config abstract factory is described in
[a blog post] (https://blog.alejandrocelaya.com/2017/07/21/reusing-factories-in-zend-servicemanager) of Alejandro Celaya
and in the [official documentation] (https://docs.zendframework.com/zend-servicemanager/config-abstract-factory).
It makes action factories superfluous. For details have a look at the blog post.