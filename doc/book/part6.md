# Part 6: Refactor application structure

In this very short part we will refactor the application structure
a little bit. We will delete the old home page and ping actions
and remove all unused files. After this the album page should be shown as
the new home page.

## Move templates

Please move the `/src/App/templates/` directory from `/src/App/` to the
root directory `/templates/`.
Please delete the `App` templates such that only the `error` and the `layout`
directories remain.
These templates are not module specific but needed application-wide.

```bash
$ mv src/App/templates .
$ rm -r templates/app
```

or use whatever tools you are comfortable with.

The layout paths are until now defined in the file `/src/App/src/ConfigProvider.php`.
In the next step, we will delete the whole module `App`, so we have to move the
`paths` configuration to a different place (and remove the `app` templates).
As the layout is application-wide and we already have a templates configuration,
we add the following lines to `/config/autoload/templates.global.php`.

```php
<?php
    /* ... */

    'templates' => [
        'paths' => [
            'error'  => [__DIR__ . '/../../templates/error'],
            'layout' => [__DIR__ . '/../../templates/layout'],
        ],
        'layout' => 'layout::default',
    ],

    /* ... */
```

## Delete unused files

Now, we can delete the following file paths and all of the files in there:

* `/src/App/`
* `/test/AppTest/`

## Remove unused configuration

Please remove the following unused configuration:

* The `App` module comes with a `CondigProvider.php` file that holds a lot of
  module specific configuration.
  As we want to remove the `App` module, we have to remove the `ConfigProvider`
  from the file `/config/config.php`:

```php
<?php
/* ... */
$aggregator = new ConfigAggregator([
    /* ... */
    // Default App module config
    App\ConfigProvider::class,
    /* ... */
], $cacheConfig['config_cache_path']);
/* ... */
```

* The `App\\` line in the `autoload` section from the `composer.json`
  file.

* The `AppTest\\` line in the `autoload-dev` section from the
  `composer.json` file.

* The `App\Action\PingAction` and `App\Action\HomePageAction`
  of the `/config/routes.php` file.

* The AlbumListAction shall be our new home page action, so add it to
  `config/routes.php`:

```php
<?php
/* ... */

$app->get('/', Album\Action\AlbumListAction::class, 'home');

/* ... */
```

## Update templates

In the navbar you can delete the menu option with the link to the `Ping Test`.

## Compare with example repository branch `part6`

You can easily compare your code with the example repository when looking
at the branch `part6`. If you want you can even clone it and have a deeper
look.

[https://gitlab.com/lowtower/zend-expressive2-tutorial/tree/part6_refactoring](https://gitlab.com/lowtower/zend-expressive2-tutorial/tree/part6_refactoring)
