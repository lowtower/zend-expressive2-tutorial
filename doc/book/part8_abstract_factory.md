# Part 8: Abstract factory

Every action needs its own factory class to inject all the depencies.
This is somewhat repetitve and cumbersome.
A reflection based abstract factory as described in [a blog post](https://xtreamwayz.com/blog/2015-12-30-psr7-abstract-action-factory-one-for-all) of xtreamwayz
makes action factories superfluous. For details have a look at the blog post.

## Add abstract factory

Let's create it in a new namespace `App` (here it comes again).
Remember the `expressive module` script from
[part2](
of the series?
Let's use it again!

```bash
$ ./vendor/bin/expressive module:create App
```
Now, we have a new registered namespace `App` in which we create the `abstract factory`
in the path `src/App/src/Http`.

```php
<?php
declare(strict_types = 1);

namespace App\Http;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Interop\Http\ServerMiddleware\MiddlewareInterface;
use ReflectionClass;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\AbstractFactoryInterface;

class AbstractActionFactory implements AbstractFactoryInterface
{
    /**
     * Create an object
     *
     * @param  ContainerInterface $container
     * @param  string             $requestedName
     * @param  null|array         $options
     *
     * @return MiddlewareInterface
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when
     *     creating a service.
     * @throws ContainerException if any other error occurs
     */
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): MiddlewareInterface {
        // Construct a new ReflectionClass object for the requested action
        $reflection = new ReflectionClass($requestedName);
        // Get the constructor
        $constructor = $reflection->getConstructor();
        if (null === $constructor) {
            // There is no constructor, just return a new class
            return new $requestedName;
        }

        // Get the parameters
        $parameters   = $constructor->getParameters();
        $dependencies = [];
        foreach ($parameters as $parameter) {
            // Get the parameter class
            $class = $parameter->getClass();
            // Get the class from the container
            $dependencies[] = $container->get($class->getName());
        }

        // Return the requested class and inject its dependencies
        return $reflection->newInstanceArgs($dependencies);
    }

    /**
     * Can the factory create an instance for the service?
     *
     * @param  ContainerInterface $container
     * @param  string             $requestedName
     *
     * @return bool
     */
    public function canCreate(ContainerInterface $container, $requestedName)
    {
        // Only accept Action classes
        if (substr($requestedName, -6) === 'Action') {
            return true;
        }

        return false;
    }
}
```

## Remove concrete factories

Now, we can remove all the action factories defined in `src/Album/src/ConfigProvider.php` and the files themselves:

```bash
$ rm src/Album/src/Action/Album*ActionFactory.php
```
or use whatever tools you are comfortable with.

```php
<?php
declare(strict_types = 1);

namespace Album;

/**
 * The configuration provider for the Album module
 *
 * @package Album
 * @see https://docs.zendframework.com/zend-component-installer
 */
class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     * @return array
     */
    public function __invoke() : array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'templates'    => $this->getTemplates(),
        ];
    }

    /**
     * Returns the container dependencies
     *
     * @return array
     */
    public function getDependencies() : array
    {
        return [
            'delegators' => [
                \Zend\Expressive\Application::class => [
                    Factory\RoutesDelegator::class,
                ],
            ],
            'factories' => [
                Model\Repository\AlbumRepositoryInterface::class =>
                    Model\Repository\AlbumRepositoryFactory::class,

                Model\Storage\AlbumStorageInterface::class =>
                    Db\AlbumTableGatewayFactory::class,

                Form\AlbumDataForm::class =>
                    Form\AlbumDataFormFactory::class,
                Form\AlbumDeleteForm::class =>
                    Form\AlbumDeleteFormFactory::class,

                Model\InputFilter\AlbumInputFilter::class =>
                    Model\InputFilter\AlbumInputFilterFactory::class,
            ],
        ];
    }

    /**
     * Returns the templates configuration
     *
     * @return array
     */
    public function getTemplates() : array
    {
        return [
            'paths' => [
                'album' => [__DIR__ . '/../templates/album'],
            ],
        ];
    }

}
```

## Add dependency

Next, we activate the abstract factory in the dependencies section of `/config/autoload/dependencies.global.php`:

```php
<?php
declare(strict_types = 1);

use Zend\Expressive\Application;
use Zend\Expressive\Container;
use Zend\Expressive\Delegate;
use Zend\Expressive\Helper;
use Zend\Expressive\Middleware;

return [
    // Provides application-wide services.
    // We recommend using fully-qualified class names whenever possible as
    // service names.
    'dependencies' => [
        // Use 'aliases' to alias a service name to another service. The
        // key is the alias name, the value is the service to which it points.
        'aliases' => [
            'Zend\Expressive\Delegate\DefaultDelegate' => Delegate\NotFoundDelegate::class,
        ],
        // Use 'invokables' for constructor-less services, or services that do
        // not require arguments to the constructor. Map a service name to the
        // class name.
        'invokables' => [
            // Fully\Qualified\InterfaceName::class => Fully\Qualified\ClassName::class,
            Helper\ServerUrlHelper::class => Helper\ServerUrlHelper::class,
        ],
        // Use 'factories' for services provided by callbacks/factory classes.
        'factories'  => [
            Application::class                => Container\ApplicationFactory::class,
            Delegate\NotFoundDelegate::class  => Container\NotFoundDelegateFactory::class,
            Helper\ServerUrlMiddleware::class => Helper\ServerUrlMiddlewareFactory::class,
            Helper\UrlHelper::class           => Helper\UrlHelperFactory::class,
            Helper\UrlHelperMiddleware::class => Helper\UrlHelperMiddlewareFactory::class,

            Zend\Stratigility\Middleware\ErrorHandler::class => Container\ErrorHandlerFactory::class,
            Middleware\ErrorResponseGenerator::class         => Container\ErrorResponseGeneratorFactory::class,
            Middleware\NotFoundHandler::class                => Container\NotFoundHandlerFactory::class,
        ],
        'abstract_factories' => [
            App\Http\AbstractActionFactory::class,
        ],
    ],
];
```

## Extract

Thanks to xtreamwayz we were able to replace all the action factories with a single abstract factory.

## Compare with example repository branch `part8`

You can easily compare your code with the example repository when looking
at the branch `part8`. If you want you can even clone it and have a deeper
look.

[https://gitlab.com/lowtower/zend-expressive2-tutorial/tree/part8_abstract_factory](https://gitlab.com/lowtower/zend-expressive2-tutorial/tree/part8_abstract_factory)
