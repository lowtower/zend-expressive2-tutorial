<?php

declare(strict_types = 1);

/*
 * Copyright (c) 2017 LowTower
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 *
 * @link https://gitlab.com/lowtower/zend-expressive2-tutorial
 */

namespace AlbumTest;

use Album\ConfigProvider;
use PHPUnit\Framework\TestCase;
use Zend\ServiceManager\AbstractFactory\ConfigAbstractFactory;

/**
 * Class ConfigProviderTest.
 *
 * @package AlbumTest
 */
class ConfigProviderTest extends TestCase
{
    /**
     * Test ConfigProvider class exists.
     */
    public function testConfigProviderClassExists()
    {
        $this->assertTrue(class_exists(ConfigProvider::class));
    }

    /**
     * Test getDependencies() returns an array.
     */
    public function testGetDependenciesReturnsArrayContainsKeys()
    {
        $configProvider = new ConfigProvider();
        $configData     = $configProvider->getDependencies();

        $this->assertInstanceOf(ConfigProvider::class, $configProvider);
        $this->assertArrayHasKey('factories', $configData, "Array key 'factories' not found!");
        $this->assertArrayHasKey('delegators', $configData, "Array key 'delegators' not found!");
    }

    /**
     * Test getTemplates() returns an array.
     */
    public function testGetTemplatesReturnsArrayContainsKeys()
    {
        $configProvider = new ConfigProvider();
        $configData     = $configProvider->getTemplates();

        $this->assertArrayHasKey('paths', $configData, "Array key 'paths' not found!");
    }

    /**
     * Test invoking object.
     */
    public function testInvokingReturnsArrayContainsKeys()
    {
        $configProvider = new ConfigProvider();
        $configData     = $configProvider();

        $this->assertArrayHasKey('dependencies', $configData, "Array key 'dependencies' not found!");
        $this->assertArrayHasKey('templates', $configData, "Array key 'templates' not found!");
        $warnMessage = "Array key '" . ConfigAbstractFactory::class . "' not found!";
        $this->assertArrayHasKey(ConfigAbstractFactory::class, $configData, $warnMessage);
    }
}
