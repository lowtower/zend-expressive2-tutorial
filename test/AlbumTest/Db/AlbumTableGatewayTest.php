<?php

declare(strict_types = 1);

/*
 * Copyright (c) 2017 LowTower
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 *
 * @link https://gitlab.com/lowtower/zend-expressive2-tutorial
 */

namespace AlbumTest\Db;

use Album\Db\AlbumTableGateway;
use Album\Model\Entity\AlbumEntity;
use Album\Model\Storage\AlbumStorageInterface;
use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\Framework\TestCase;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\Sql\Select;
use Zend\Hydrator\ArraySerializable;

class AlbumTableGatewayTest extends TestCase
{
    use TestCaseTrait;

    /**
     * @var AlbumTableGateway
     */
    private $tableGateway;

    /**
     * @var AlbumStorageInterface
     */
    private $albumStorage;

    /**
     * @var Adapter
     */
    private $adapter;

    /**
     * @var \PHPUnit\DbUnit\Database\Connection
     */
    private $connection;

    /**
     * @var array
     */
    private $albumData = [
        1 => [
            'id'     => '1',
            'artist' => 'The Military Wives',
            'title'  => 'In My Dreams',
        ],
        2 => [
            'id'     => '2',
            'artist' => 'Adele',
            'title'  => '21',
        ],
        3 => [
            'id'     => '3',
            'artist' => 'Bruce Springsteen',
            'title'  => 'Wrecking Ball (Deluxe)',
        ],
        4 => [
            'id'     => '4',
            'artist' => 'Lana Del Rey',
            'title'  => 'Born To Die',
        ],
        5 => [
            'id'     => '5',
            'artist' => 'Gotye',
            'title'  => 'Making Mirrors',
        ],
    ];

    /**
     * Sets up the test.
     */
    protected function setUp()
    {
        $dbConfig           = include __DIR__ . '/../../../config/autoload/database.test.php';
        $this->adapter      = new Adapter($dbConfig['db']);
        $resultSet          = new HydratingResultSet(new ArraySerializable(), new AlbumEntity());
        $this->tableGateway = new AlbumTableGateway($this->adapter, $resultSet);
        parent::setUp();
    }

    /**
     * Returns the test database connection.
     *
     * @return \PHPUnit\DbUnit\Database\DefaultConnection
     */
    public function getConnection()
    {
        //         $pdo = new \PDO('sqlite::memory:');
        //         return $this->createDefaultDBConnection($pdo, ':memory:');
        if (!$this->connection) {
            $this->connection = $this->createDefaultDBConnection(
                $this->adapter->getDriver()->getConnection()->getResource(
                ),
                'album'
            );
        }

        return $this->connection;
    }

    /**
     * Returns the test dataset.
     *
     * @return \PHPUnit\DbUnit\DataSet\IDataSet
     */
    protected function getDataSet()
    {
        return $this->createXMLDataSet(__DIR__ . '/../assets/album-test-data.xml');
    }

    /**
     * Test fetch all albums.
     *
     * @param $id
     *
     * @dataProvider provideAlbumByIdData
     */
    public function testFetchAllAlbums($id)
    {
        $queryTable = $this->getConnection()->createQueryTable(
            'fetchAllAlbums',
            'SELECT * FROM `album`;'
        );
        $allAlbums = [];
        for ($key = 0; $key < $queryTable->getRowCount(); ++$key) {
            $album                   = $queryTable->getRow($key);
            $allAlbums[$album['id']] = $album;
        }

        $albumList  = $this->tableGateway->fetchAlbumList(false);
        foreach ($albumList as $album) {
            $this->assertTrue(in_array($album->getId(), array_keys($allAlbums), false));
        }

        $this->assertEquals(155, $this->getConnection()->getRowCount('album'));
        $this->assertInstanceOf(AlbumEntity::class, $albumList[$id]);
    }

    /**
     * Test fetch all albums.
     *
     * @param $id
     *
     * @dataProvider provideAlbumByIdData
     */
    public function testFetchAllAlbumsPaginated($id)
    {
        $queryTable = $this->getConnection()->createQueryTable(
            'fetchAllAlbums',
            'SELECT * FROM `album`;'
        );
        $allAlbums = [];
        for ($key = 0; $key < $queryTable->getRowCount(); ++$key) {
            $album                   = $queryTable->getRow($key);
            $allAlbums[$album['id']] = $album;
        }

        $paginator = $this->tableGateway->fetchAlbumList(true);
        foreach ($paginator as $album) {
            $this->assertTrue(in_array($album->getId(), array_keys($allAlbums), false));
        }

        $this->assertEquals(155, $paginator->getTotalItemCount());
        $this->assertInstanceOf(AlbumEntity::class, $paginator->getItem($id));
    }

    /**
     * Test fetch album by id.
     *
     * @param $id
     *
     * @dataProvider provideAlbumByIdData
     */
    public function testFetchAlbumById($id)
    {
        $albumById  = $this->tableGateway->fetchAlbumById($id);
        $queryTable = $this->getConnection()->createQueryTable(
            'fetchAlbumById',
            'SELECT * FROM `album` WHERE id = "' . $id . '";'
        );
        $this->assertEquals($queryTable->getRow(0)['id'], $albumById->getId());
        $this->assertInstanceOf(AlbumEntity::class, $albumById);
    }

    /**
     * Test insert album.
     */
    public function testInsertAlbum()
    {
        $entity = new AlbumEntity();
        $entity->exchangeArray([
            'title'  => 'testTitle',
            'artist' => 'testArtist',
        ]);
        $this->assertEquals(155, $this->getConnection()->getRowCount('album'));
        $album = $this->tableGateway->insertAlbum($entity);
        $this->assertTrue($album);
        $this->assertEquals(156, $this->getConnection()->getRowCount('album'));
    }

    /**
     * Test update album.
     */
    public function testUpdateAlbum()
    {
        $id        = 1;
        $albumById = $this->tableGateway->fetchAlbumById($id);
        $this->assertEquals($this->albumData[$id]['artist'], $albumById->getArtist());

        $entity = new AlbumEntity();
        $entity->exchangeArray([
            'id'     => '1',
            'title'  => 'testTitle',
            'artist' => 'testArtist',
        ]);
        $album     = $this->tableGateway->updateAlbum($entity);
        $albumById = $this->tableGateway->fetchAlbumById($id);
        $this->assertTrue($album);
        $this->assertEquals('testArtist', $albumById->getArtist());

        $entity = new AlbumEntity();
        $entity->exchangeArray($this->albumData[$id]);

        $album     = $this->tableGateway->updateAlbum($entity);
        $albumById = $this->tableGateway->fetchAlbumById($id);
        $this->assertTrue($album);
        $this->assertEquals($this->albumData[$id]['artist'], $albumById->getArtist());
    }

    /**
     * Test delete album.
     *
     * @param $id
     */
    public function testDeleteAlbum()
    {
        $albumList  = $this->tableGateway->fetchAlbumList(false);
        end($albumList);
        $lastRowId = $albumList[key($albumList)]->getId();

        $entity = new AlbumEntity();
        $entity->exchangeArray([
            'id'     => $lastRowId,
            'title'  => 'testTitle',
            'artist' => 'testArtist',
        ]);
        $this->assertEquals(156, $this->getConnection()->getRowCount('album'));
        $album = $this->tableGateway->deleteAlbum($entity);
        $this->assertTrue($album);
        $this->assertEquals(155, $this->getConnection()->getRowCount('album'));
    }

    /**
     * Data provider for albums sorted tests.
     *
     * @return array
     */
    public function provideAlbumByIdData()
    {
        return [
            [3],
            [1],
            [4],
            [2],
            [5],
        ];
    }
}
