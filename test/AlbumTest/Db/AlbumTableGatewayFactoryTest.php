<?php

declare(strict_types = 1);

/*
 * Copyright (c) 2017 LowTower
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 *
 * @link https://gitlab.com/lowtower/zend-expressive2-tutorial
 */

namespace AlbumTest\Db;

use Album\Db\AlbumTableGateway;
use Album\Db\AlbumTableGatewayFactory;
use Interop\Container\ContainerInterface;
use PHPUnit\Framework\TestCase;
use Zend\Db\Adapter\AdapterInterface;

class AlbumTableGatewayFactoryTest extends TestCase
{
    public function testFactory()
    {
        /** @var AlbumStorageInterface $albumStorage */
        $dbAdapter = $this->prophesize(AdapterInterface::class);

        /** @var ContainerInterface $container */
        $container = $this->prophesize(ContainerInterface::class);
        $container->get(AdapterInterface::class)
            ->willReturn($dbAdapter)
            ->shouldBeCalled();

        $factory = new AlbumTableGatewayFactory();

        $this->assertInstanceOf(AlbumTableGatewayFactory::class, $factory);

        /** @var AlbumDbStorage $table */
        $table = $factory(
            $container->reveal()
        );

        $this->assertInstanceOf(AlbumTableGateway::class, $table);
    }
}
