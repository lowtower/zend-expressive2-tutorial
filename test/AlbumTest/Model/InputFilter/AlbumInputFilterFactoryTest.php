<?php

declare(strict_types = 1);

/*
 * Copyright (c) 2017 LowTower
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 *
 * @link https://gitlab.com/lowtower/zend-expressive2-tutorial
 */

namespace AlbumTest\Model\InputFilter;

use Album\Model\InputFilter\AlbumInputFilter;
use Album\Model\InputFilter\AlbumInputFilterFactory;
use Interop\Container\ContainerInterface;
use PHPUnit\Framework\TestCase;

class AlbumInputFilterFactoryTest extends TestCase
{
    public function testFactory()
    {
        /** @var ContainerInterface $container */
        $container = $this->prophesize(ContainerInterface::class);

        $factory = new AlbumInputFilterFactory();

        /** @var AlbumInputFilter $inputFilter */
        $inputFilter = $factory($container->reveal());
        $this->assertInstanceof(AlbumInputFilter::class, $inputFilter);
    }
}
