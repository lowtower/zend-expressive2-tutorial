<?php

declare(strict_types = 1);

/*
 * Copyright (c) 2017 LowTower
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 *
 * @link https://gitlab.com/lowtower/zend-expressive2-tutorial
 */

namespace AlbumTest\Model\Repository;

use Album\Model\Repository\AlbumRepository;
use Album\Model\Repository\AlbumRepositoryFactory;
use Album\Model\Storage\AlbumStorageInterface;
use Interop\Container\ContainerInterface;
use PHPUnit\Framework\TestCase;

class AlbumRepositoryFactoryTest extends TestCase
{
    public function testFactory()
    {
        /** @var AlbumStorageInterface $albumTable */
        $albumTable = $this->prophesize(AlbumStorageInterface::class);

        /** @var ContainerInterface $container */
        $container = $this->prophesize(ContainerInterface::class);
        $container->get(AlbumStorageInterface::class)
            ->willReturn($albumTable)
            ->shouldBeCalled();

        $factory = new AlbumRepositoryFactory();

        /** @var AlbumRepository $repository */
        $repository = $factory($container->reveal());
        $this->assertInstanceof(AlbumRepository::class, $repository);
    }
}
