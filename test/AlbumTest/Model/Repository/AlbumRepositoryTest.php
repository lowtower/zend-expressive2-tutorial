<?php

declare(strict_types = 1);

/*
 * Copyright (c) 2017 LowTower
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 *
 * @link https://gitlab.com/lowtower/zend-expressive2-tutorial
 */

namespace AlbumTest\Model\Repository;

use Album\Model\Entity\AlbumEntity;
use Album\Model\Repository\AlbumRepository;
use Album\Model\Repository\AlbumRepositoryInterface;
use Album\Model\Storage\AlbumStorageInterface;
use PHPUnit\Framework\TestCase;
use Zend\Paginator\Adapter\ArrayAdapter;
use Zend\Paginator\Paginator;

class AlbumRepositoryTest extends TestCase
{
    /**
     * @var AlbumRepositoryInterface
     */
    private $albumRepository;

    /**
     * @var AlbumStorageInterface
     */
    private $albumTable;

    /**
     * @var array
     */
    private $albumData = [
        [
            'id'     => '1',
            'artist' => 'The Military Wives',
            'title'  => 'In My Dreams',
        ],
        [
            'id'     => '2',
            'artist' => 'Adele',
            'title'  => '21',
        ],
        [
            'id'     => '3',
            'artist' => 'Bruce Springsteen',
            'title'  => 'Wrecking Ball (Deluxe)',
        ],
    ];

    /**
     * Sets up the test.
     */
    protected function setUp()
    {
        $this->albumTable = $this->prophesize(
            AlbumStorageInterface::class
        );
        $this->albumRepository = new AlbumRepository($this->albumTable->reveal());
    }

    /**
     * Test get all albums.
     */
    public function testFetchAllUnpaginatedReturnsAllAlbums()
    {
        $expectedData = $this->albumData;
        $this->albumTable
            ->fetchAlbumList(false)
            ->willReturn($expectedData)
            ->shouldBeCalled();
        $this->assertEquals(
            $expectedData,
            $this->albumRepository->fetchAllAlbums(false)
        );
    }

    /**
     * Test get all albums.
     */
    public function testFetchAllPaginatedReturnsAllAlbums()
    {
        $expectedData = new Paginator(new ArrayAdapter($this->albumData));
        $this->albumTable
            ->fetchAlbumList(true)
            ->willReturn($expectedData)
            ->shouldBeCalled();
        $this->assertEquals(
            $expectedData,
            $this->albumRepository->fetchAllAlbums(true)
        );
    }

    /**
     * Test get single album.
     */
    public function testFetchSingleAlbumReturnsSingleAlbum()
    {
        $albumData    = $this->albumData[0];
        $expectedData = $albumData;
        $this->albumTable
            ->fetchAlbumById($albumData['id'])
            ->willReturn($albumData)
            ->shouldBeCalled();
        $this->assertEquals(
            $expectedData,
            $this->albumRepository->fetchSingleAlbum($albumData['id'])
        );
    }

    /**
     * Test update album.
     */
    public function testSaveAlbumWillUpdateExistingAlbumsIfTheyAlreadyHaveAnId()
    {
        $entity = new AlbumEntity();
        $data   = $this->albumData[0];
        $entity->exchangeArray($data);

        $this->albumTable
            ->updateAlbum($entity)
            ->willReturn(true)
            ->shouldBeCalled();
        $this->assertTrue($this->albumRepository->saveAlbum($entity));
    }

    /**
     * Test insert album.
     */
    public function testSaveAlbumWillInsertNewAlbumsIfTheyDontAlreadyHaveAnId()
    {
        $entity         = new AlbumEntity();
        $data['artist'] = $this->albumData[0]['artist'];
        $data['title']  = $this->albumData[0]['title'];
        $entity->exchangeArray($data);

        $this->albumTable
            ->insertAlbum($entity)
            ->willReturn(true)
            ->shouldBeCalled();
        $this->assertTrue($this->albumRepository->saveAlbum($entity));
    }

    /**
     * Test delete album.
     */
    public function testCanDeleteAnAlbumByItsEntitiesId()
    {
        $entity = new AlbumEntity();
        $data   = $this->albumData[0];
        $entity->exchangeArray($data);

        $this->albumTable
            ->deleteAlbum($entity)
            ->willReturn(true)
            ->shouldBeCalled();
        $this->assertTrue($this->albumRepository->deleteAlbum($entity));
    }
}
