<?php

declare(strict_types = 1);

/*
 * Copyright (c) 2017 LowTower
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 *
 * @link https://gitlab.com/lowtower/zend-expressive2-tutorial
 */

namespace AlbumTest\Factory;

use Album\Action;
use Album\Factory\RoutesDelegator;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Zend\Expressive\Application;

/**
 * Class RoutesDelegatorTest.
 *
 * @package AlbumTest
 */
class RoutesDelegatorTest extends TestCase
{
    /**
     * Test Album routes are registered.
     *
     * @group  functional
     */
    public function testRegistersAlbumRoutes()
    {
        $container = $this->prophesize(ContainerInterface::class)->reveal();

        $app = $this->prophesize(Application::class);
        $app->get('/album', Action\AlbumListAction::class, 'album')
            ->shouldBeCalled();
        $app->get('/album/create', Action\AlbumCreateFormAction::class, 'album-create')
            ->shouldBeCalled();
        $app->post('/album/create/handle', [Action\AlbumCreateHandleAction::class,
                                                      Action\AlbumCreateFormAction::class,
                                                    ], 'album-create-handle')
            ->shouldBeCalled();
        $app->get("/album/update/{id:\d+}", Action\AlbumUpdateFormAction::class, 'album-update')
            ->shouldBeCalled();
        $app->post("/album/update/{id:\d+}/handle", [Action\AlbumUpdateHandleAction::class,
                                                      Action\AlbumUpdateFormAction::class,
                                                    ], 'album-update-handle')
            ->shouldBeCalled();
        $app->get("/album/delete/{id:\d+}", Action\AlbumDeleteFormAction::class, 'album-delete')
            ->shouldBeCalled();
        $app->post("/album/delete/{id:\d+}/handle", [Action\AlbumDeleteHandleAction::class,
                                                      Action\AlbumDeleteFormAction::class,
                                                    ], 'album-delete-handle')
            ->shouldBeCalled();

        $callback = function () use ($app) {
            return $app->reveal();
        };

        $delegator = new RoutesDelegator();

        $result = $delegator($container, Application::class, $callback);

        $this->assertSame($app->reveal(), $result);
    }
}
