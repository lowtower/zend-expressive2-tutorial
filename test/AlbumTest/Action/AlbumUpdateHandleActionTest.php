<?php

declare(strict_types = 1);

/*
 * Copyright (c) 2017 LowTower
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 *
 * @link https://gitlab.com/lowtower/zend-expressive2-tutorial
 */

namespace AlbumTest\Action;

use Album\Action\AlbumUpdateHandleAction;
use Album\Form\AlbumDataForm;
use Album\Model\Entity\AlbumEntity;
use Album\Model\Repository\AlbumRepositoryInterface;
use Interop\Http\ServerMiddleware\DelegateInterface;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;

class AlbumUpdateHandleActionTest extends TestCase
{
    /**
     * @var array
     */
    private $albumData;

    /**
     * @var AlbumEntity
     */
    private $entity;

    /**
     * Setup test case.
     */
    protected function setUp()
    {
        $this->albumData = [
            'id'     => 2,
            'artist' => 'Adele',
            'title'  => '21',
        ];

        $this->entity = new AlbumEntity();
        $this->entity->exchangeArray($this->albumData);
    }

    /**
     * Test if action returns RedirectResponse.
     */
    public function testActionAlbumUpdateHandleWithValidFormReturnsRedirectResponse()
    {
        $this->albumData['artist'] = 'NotAdele';

        $srStub = $this->prophesize(ServerRequestInterface::class);
        $srStub->getAttribute('id')
            ->shouldBeCalled()
            ->willReturn(2);
        $srStub->getParsedBody()
            ->shouldBeCalled()
            ->willReturn($this->albumData);

        $routerStub = $this->prophesize(RouterInterface::class);
        $routerStub->generateUri('album')
            ->shouldBeCalled()
            ->willReturn('/album');

        $albumRepository = $this->prophesize(AlbumRepositoryInterface::class);
        $albumRepository->fetchSingleAlbum(2)
            ->shouldBeCalled()
            ->willReturn($this->entity);

        $this->assertEquals('Adele', $this->entity->getArtist());

        $albumForm = $this->prophesize(AlbumDataForm::class);
        $albumForm->setData($this->albumData)
            ->shouldBeCalled();
        $albumForm->isValid()
            ->shouldBeCalled()
            ->willReturn(true);

        $albumRepository->saveAlbum($this->entity)
            ->shouldBeCalled()
            ->willReturn(true);

        $action = new AlbumUpdateHandleAction(
            $routerStub->reveal(),
            $albumRepository->reveal(),
            $albumForm->reveal()
        );

        $response = $action->process(
            $srStub->reveal(),
            $this->prophesize(DelegateInterface::class)->reveal()
        );

        $this->assertInstanceOf(RedirectResponse::class, $response);
        $this->assertInstanceOf(AlbumEntity::class, $albumRepository->reveal()->fetchSingleAlbum(2));
        $this->assertEquals('NotAdele', $albumRepository->reveal()->fetchSingleAlbum(2)->getArtist());
    }

    /**
     * Test if action returns DelegateInterface.
     */
    public function testActionAlbumUpdateHandleWithInvalidFormReturnsDelegateInterface()
    {
        $srStub = $this->prophesize(ServerRequestInterface::class);
        $srStub->getAttribute('id')
            ->shouldBeCalled()
            ->willReturn(2);
        $srStub->getParsedBody()
            ->shouldBeCalled()
            ->willReturn($this->albumData);

        $riStub = $this->prophesize(ResponseInterface::class);
        $riStub->getStatusCode()
            ->willReturn(404);
        $dlStub = $this->prophesize(DelegateInterface::class);
        $dlStub->process($srStub)
            ->willReturn($riStub->reveal());

        $albumForm = $this->prophesize(AlbumDataForm::class);
        $albumForm->setData($this->albumData)
            ->shouldBeCalled();
        $albumForm->isValid()
            ->shouldBeCalled()
            ->willReturn(false);

        $action = new AlbumUpdateHandleAction(
            $this->prophesize(RouterInterface::class)->reveal(),
            $this->prophesize(AlbumRepositoryInterface::class)->reveal(),
            $albumForm->reveal()
        );

        $response = $action->process(
            $srStub->reveal(),
            $dlStub->reveal()
        );

        $this->assertInstanceOf(ResponseInterface::class, $response);
        $this->assertEquals($response->getStatusCode(), 404);
    }
}
