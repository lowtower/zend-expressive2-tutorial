<?php

declare(strict_types = 1);

/*
 * Copyright (c) 2017 LowTower
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 *
 * @link https://gitlab.com/lowtower/zend-expressive2-tutorial
 */

namespace AlbumTest\Action;

use Album\Action\AlbumDeleteHandleAction;
use Album\Model\Entity\AlbumEntity;
use Album\Model\Repository\AlbumRepositoryInterface;
use Interop\Http\ServerMiddleware\DelegateInterface;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;

class AlbumDeleteHandleActionTest extends TestCase
{
    /**
     * @var array
     */
    private $albumData;

    /**
     * @var AlbumEntity
     */
    private $entity;

    /**
     * Setup test case.
     */
    protected function setUp()
    {
        $this->albumData = [
            'id'     => 2,
            'artist' => 'Adele',
            'title'  => '21',
        ];

        $this->entity = new AlbumEntity();
        $this->entity->exchangeArray($this->albumData);
    }

    /**
     * Test if action returns RedirectResponse.
     */
    public function testActionAlbumDeleteHandleWithFormYesCallsDeleteAlbumReturnsRedirectResponse()
    {
        $this->albumData['delete_album_yes'] = 'Yes';

        $srStub = $this->prophesize(ServerRequestInterface::class);
        $srStub->getAttribute('id')
            ->shouldBeCalled()
            ->willReturn(2);
        $srStub->getParsedBody()
            ->shouldBeCalled()
            ->willReturn($this->albumData);

        $routerStub = $this->prophesize(RouterInterface::class);
        $routerStub->generateUri('album')
            ->shouldBeCalled()
            ->willReturn('/album');

        $albumRepository = $this->prophesize(AlbumRepositoryInterface::class);
        $albumRepository->fetchSingleAlbum(2)
            ->shouldBeCalled()
            ->willReturn($this->entity);
        $albumRepository->deleteAlbum($this->entity)
            ->shouldBeCalled();

        $action = new AlbumDeleteHandleAction(
            $routerStub->reveal(),
            $albumRepository->reveal()
        );

        $response = $action->process(
            $srStub->reveal(),
            $this->prophesize(DelegateInterface::class)->reveal()
        );

        $this->assertInstanceOf(RedirectResponse::class, $response);
        $this->assertInstanceOf(AlbumEntity::class, $albumRepository->reveal()->fetchSingleAlbum(2));
    }

    /**
     * Test if action returns RedirectResponse.
     */
    public function testActionAlbumDeleteHandleWithFormNoCallsNotDeleteAlbumReturnsRedirectResponse()
    {
        $this->albumData['delete_album_no'] = 'No';

        $srStub = $this->prophesize(ServerRequestInterface::class);
        $srStub->getAttribute('id')
            ->shouldBeCalled()
            ->willReturn(2);
        $srStub->getParsedBody()
            ->shouldBeCalled()
            ->willReturn($this->albumData);

        $routerStub = $this->prophesize(RouterInterface::class);
        $routerStub->generateUri('album')
            ->shouldBeCalled()
            ->willReturn('/album');

        $albumRepository = $this->prophesize(AlbumRepositoryInterface::class);
        $albumRepository->fetchSingleAlbum(2)
            ->shouldBeCalled()
            ->willReturn($this->entity);
        $albumRepository->deleteAlbum($this->entity)
            ->shouldNotBeCalled();

        $action = new AlbumDeleteHandleAction(
            $routerStub->reveal(),
            $albumRepository->reveal()
        );

        $response = $action->process(
            $srStub->reveal(),
            $this->prophesize(DelegateInterface::class)->reveal()
        );

        $this->assertInstanceOf(RedirectResponse::class, $response);
        $this->assertInstanceOf(AlbumEntity::class, $albumRepository->reveal()->fetchSingleAlbum(2));
    }
}
