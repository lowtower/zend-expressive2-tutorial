<?php

declare(strict_types = 1);

/*
 * Copyright (c) 2017 LowTower
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 *
 * @link https://gitlab.com/lowtower/zend-expressive2-tutorial
 */

namespace AlbumTest\Action;

use Album\Action\AlbumCreateFormAction;
use Album\Form\AlbumDataForm;
use Interop\Http\ServerMiddleware\DelegateInterface;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template\TemplateRendererInterface;

class AlbumCreateFormActionTest extends TestCase
{
    /**
     * Test if action renders the album form.
     */
    public function testActionRendersAlbumCreateFormWithMessageTemplate()
    {
        $albumForm = $this->prophesize(AlbumDataForm::class);
        $albumForm->getMessages()
            ->shouldBeCalled()
            ->willReturn([
                'any error message',
            ]);

        $renderer = $this->prophesize(TemplateRendererInterface::class);
        $renderer->render(
            'album::create',
            ['albumForm' => $albumForm,
             'message'   => 'Please check your input!',
            ]
        )
            ->shouldBeCalled()
            ->willReturn('BODY');

        $action = new AlbumCreateFormAction(
            $renderer->reveal(),
            $albumForm->reveal()
        );

        $response = $action->process(
            $this->prophesize(ServerRequestInterface::class)->reveal(),
            $this->prophesize(DelegateInterface::class)->reveal()
        );

        $this->assertInstanceOf(HtmlResponse::class, $response);
        $this->assertEquals('BODY', $response->getBody());
    }

    /**
     * Test if action renders the album form.
     */
    public function testActionRendersAlbumCreateFormWithoutMessageTemplate()
    {
        $albumForm = $this->prophesize(AlbumDataForm::class);
        $albumForm->getMessages()
            ->shouldBeCalled()
            ->willReturn(null);

        $renderer = $this->prophesize(TemplateRendererInterface::class);
        $renderer->render(
            'album::create',
            ['albumForm' => $albumForm,
             'message'   => 'Please enter the new album!',
            ]
        )
            ->shouldBeCalled()
            ->willReturn('BODY');

        $action = new AlbumCreateFormAction(
            $renderer->reveal(),
            $albumForm->reveal()
        );

        $response = $action->process(
            $this->prophesize(ServerRequestInterface::class)->reveal(),
            $this->prophesize(DelegateInterface::class)->reveal()
        );

        $this->assertInstanceOf(HtmlResponse::class, $response);
        $this->assertEquals('BODY', $response->getBody());
    }
}
