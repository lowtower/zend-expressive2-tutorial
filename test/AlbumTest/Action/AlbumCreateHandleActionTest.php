<?php

declare(strict_types = 1);

/*
 * Copyright (c) 2017 LowTower
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 *
 * @link https://gitlab.com/lowtower/zend-expressive2-tutorial
 */

namespace AlbumTest\Action;

use Album\Action\AlbumCreateHandleAction;
use Album\Form\AlbumDataForm;
use Album\Model\Entity\AlbumEntity;
use Album\Model\Repository\AlbumRepositoryInterface;
use Interop\Http\ServerMiddleware\DelegateInterface;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Expressive\Router\RouterInterface;

class AlbumCreateHandleActionTest extends TestCase
{
    /**
     * Test if action returns RedirectResponse.
     */
    public function testActionAlbumCreateHandleWithValidFormReturnsRedirectResponse()
    {
        $albumData = [
            'save_album' => 'Save Album',
            'artist'     => 'testArtist',
            'title'      => 'testTitle',
        ];
        $entity = new AlbumEntity();
        $entity->exchangeArray($albumData);

        $srStub = $this->prophesize(ServerRequestInterface::class);
        $srStub->getParsedBody()
            ->shouldBeCalled()
            ->willReturn($albumData);

        $routerStub = $this->prophesize(RouterInterface::class);
        $routerStub->generateUri('album')
            ->shouldBeCalled()
            ->willReturn('/album');

        $albumRepository = $this->prophesize(AlbumRepositoryInterface::class);
        $albumRepository->saveAlbum($entity)
            ->shouldBeCalled()
            ->willReturn(true);

        $albumForm = $this->prophesize(AlbumDataForm::class);
        $albumForm->setData($albumData)
            ->shouldBeCalled();
        $albumForm->isValid()
            ->shouldBeCalled()
            ->willReturn(true);

        $action = new AlbumCreateHandleAction(
            $routerStub->reveal(),
            $albumRepository->reveal(),
            $albumForm->reveal()
        );

        $response = $action->process(
            $srStub->reveal(),
            $this->prophesize(DelegateInterface::class)->reveal()
        );

        $this->assertInstanceOf(RedirectResponse::class, $response);
    }

    /**
     * Test if action returns DelegateInterface.
     */
    public function testActionAlbumCreateHandleWithInvalidFormReturnsDelegateInterface()
    {
        $albumData = [
            'save_album' => 'Save Album',
            'artist'     => 'testArtist',
            'title'      => 'testTitle',
        ];

        $srStub = $this->prophesize(ServerRequestInterface::class);
        $srStub->getParsedBody()
            ->shouldBeCalled()
            ->willReturn($albumData);

        $riStub = $this->prophesize(ResponseInterface::class);
        $riStub->getStatusCode()
            ->willReturn(404);
        $dlStub = $this->prophesize(DelegateInterface::class);
        $dlStub->process($srStub)
            ->willReturn($riStub->reveal());

        $albumForm = $this->prophesize(AlbumDataForm::class);
        $albumForm->setData($albumData)
            ->shouldBeCalled();
        $albumForm->isValid()
            ->shouldBeCalled()
            ->willReturn(false);

        $action = new AlbumCreateHandleAction(
            $this->prophesize(RouterInterface::class)->reveal(),
            $this->prophesize(AlbumRepositoryInterface::class)->reveal(),
            $albumForm->reveal()
        );

        $response = $action->process(
            $srStub->reveal(),
            $dlStub->reveal()
        );

        $this->assertInstanceOf(ResponseInterface::class, $response);
        $this->assertEquals($response->getStatusCode(), 404);
    }
}
