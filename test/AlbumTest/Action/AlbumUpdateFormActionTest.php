<?php

declare(strict_types = 1);

/*
 * Copyright (c) 2017 LowTower
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 *
 * @link https://gitlab.com/lowtower/zend-expressive2-tutorial
 */

namespace AlbumTest\Action;

use Album\Action\AlbumUpdateFormAction;
use Album\Form\AlbumDataForm;
use Album\Model\Entity\AlbumEntity;
use Album\Model\Repository\AlbumRepositoryInterface;
use Interop\Http\ServerMiddleware\DelegateInterface;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Expressive\Template\TemplateRendererInterface;

class AlbumUpdateFormActionTest extends TestCase
{
    /**
     * @var AlbumEntity
     */
    private $entity;

    /**
     * Setup test case.
     */
    protected function setUp()
    {
        $this->entity = new AlbumEntity();
        $this->entity->exchangeArray([
            'id'     => 2,
            'artist' => 'Adele',
            'title'  => '21',
        ]);
    }

    /**
     * Test if action renders the album form.
     */
    public function testActionRendersTemplateAlbumUpdateFormWithMessage()
    {
        $srStub = $this->prophesize(ServerRequestInterface::class);
        $srStub->getAttribute('id')
            ->shouldBeCalled()
            ->willReturn(2);

        $albumForm = $this->prophesize(AlbumDataForm::class);
        $albumForm->getMessages()
            ->shouldBeCalled()
            ->willReturn([
                'any error message',
            ]);

        $renderer = $this->prophesize(TemplateRendererInterface::class);
        $renderer->render(
            'album::update',
            ['albumForm'   => $albumForm,
             'albumEntity' => $this->entity,
             'message'     => 'Please check your input!',
            ]
        )
            ->shouldBeCalled()
            ->willReturn('BODY');

        $albumRepository = $this->prophesize(AlbumRepositoryInterface::class);
        $albumRepository->fetchSingleAlbum(2)
            ->shouldBeCalled()
            ->willReturn($this->entity);

        $action = new AlbumUpdateFormAction(
            $renderer->reveal(),
            $albumRepository->reveal(),
            $albumForm->reveal()
        );

        $response = $action->process(
            $srStub->reveal(),
            $this->prophesize(DelegateInterface::class)->reveal()
        );

        $this->assertInstanceOf(HtmlResponse::class, $response);
        $this->assertEquals('BODY', $response->getBody());
    }

    /**
     * Test if action renders the album form.
     */
    public function testActionRendersTemplateAlbumUpdateFormWithoutMessage()
    {
        $srStub = $this->prophesize(ServerRequestInterface::class);
        $srStub->getAttribute('id')
            ->shouldBeCalled()
            ->willReturn(2);

        $albumForm = $this->prophesize(AlbumDataForm::class);
        $albumForm->getMessages()
            ->shouldBeCalled()
            ->willReturn(null);
        $albumForm->bind($this->entity)
            ->shouldBeCalled();

        $renderer = $this->prophesize(TemplateRendererInterface::class);
        $renderer->render(
            'album::update',
            ['albumForm'   => $albumForm,
             'albumEntity' => $this->entity,
             'message'     => 'Please change the album!',
            ]
        )
            ->shouldBeCalled()
            ->willReturn('BODY');

        $albumRepository = $this->prophesize(AlbumRepositoryInterface::class);
        $albumRepository->fetchSingleAlbum(2)
            ->shouldBeCalled()
            ->willReturn($this->entity);

        $action = new AlbumUpdateFormAction(
            $renderer->reveal(),
            $albumRepository->reveal(),
            $albumForm->reveal()
        );

        $response = $action->process(
            $srStub->reveal(),
            $this->prophesize(DelegateInterface::class)->reveal()
        );

        $this->assertInstanceOf(HtmlResponse::class, $response);
        $this->assertEquals('BODY', $response->getBody());
    }
}
