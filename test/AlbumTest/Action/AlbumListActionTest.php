<?php

declare(strict_types = 1);

/*
 * Copyright (c) 2017 LowTower
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 *
 * @link https://gitlab.com/lowtower/zend-expressive2-tutorial
 */

namespace AlbumTest\Action;

use Album\Action\AlbumListAction;
use Interop\Http\ServerMiddleware\DelegateInterface;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Uri;
use Zend\Expressive\Template\TemplateRendererInterface;
use Zend\Paginator\Paginator;

class AlbumListActionTest extends TestCase
{
    /**
     * Test if action renders the album list.
     */
    public function testActionRendersAlbumListTemplate()
    {
        // Mock the paginator, disabling the constructor
        $paginatorMock = $this->getMockBuilder(
            '\Zend\Paginator\Paginator',
            ['setCurrentPageNumber', 'setItemCountPerPage'],
            [],
            '',
            false
        )
            ->disableOriginalConstructor()
            ->getMock();
        $paginatorMock->expects($this->any())
            ->method('setCurrentPageNumber')
            ->with($this->equalTo(123))
            ->willReturn($paginatorMock);
        $paginatorMock->expects($this->any())
            ->method('setItemCountPerPage')
            ->with($this->equalTo(10))
            ->willReturn($paginatorMock);

        // Mock the repository, disabling the constructor
        $albumRepoMock = $this->getMockBuilder(
            '\Album\Model\Repository\AlbumRepositoryInterface',
            ['fetchAllAlbums'],
            [],
            '',
            false
        )
            ->disableOriginalConstructor()
            ->getMock();
        $albumRepoMock->expects($this->any())
            ->method('fetchAllAlbums')
            ->with($this->equalTo(true))
            ->willReturn($paginatorMock);

        $renderer = $this->prophesize(TemplateRendererInterface::class);
        $renderer->render(
            'album::list',
            ['paginator' => $paginatorMock]
        )
            ->shouldBeCalled()
            ->willReturn('BODY');

        $action = new AlbumListAction(
            $renderer->reveal(),
            $albumRepoMock
        );

        // Stub the ServerRequestInterface
        $srStub = $this->prophesize(ServerRequestInterface::class);
        $srStub->getUri()->willReturn(new Uri('http://www.test.com?page=123'));

        $response = $action->process(
            $srStub->reveal(),
            $this->prophesize(DelegateInterface::class)->reveal()
        );

        $this->assertInstanceOf(HtmlResponse::class, $response);
        $this->assertEquals('BODY', $response->getBody());
    }
}
