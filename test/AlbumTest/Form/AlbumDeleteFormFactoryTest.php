<?php

declare(strict_types = 1);

/*
 * Copyright (c) 2017 LowTower
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 *
 * @link https://gitlab.com/lowtower/zend-expressive2-tutorial
 */

namespace AlbumTest\Form;

use Album\Form\AlbumDeleteForm;
use Album\Form\AlbumDeleteFormFactory;
use Interop\Container\ContainerInterface;
use PHPUnit\Framework\TestCase;

class AlbumDeleteFormFactoryTest extends TestCase
{
    public function testFactory()
    {
        /** @var ContainerInterface $container */
        $container = $this->prophesize(ContainerInterface::class);

        $factory = new AlbumDeleteFormFactory();

        /** @var AlbumDeleteForm $deleteForm */
        $deleteForm = $factory($container->reveal());
        $this->assertInstanceof(AlbumDeleteForm::class, $deleteForm);
        $this->assertEquals('submit', $deleteForm->get('delete_album_yes')->getAttribute('type'));
        $this->assertEquals('submit', $deleteForm->get('delete_album_no')->getAttribute('type'));
    }
}
