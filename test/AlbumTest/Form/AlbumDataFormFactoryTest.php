<?php

declare(strict_types = 1);

/*
 * Copyright (c) 2017 LowTower
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 *
 * @link https://gitlab.com/lowtower/zend-expressive2-tutorial
 */

namespace AlbumTest\Form;

use Album\Form\AlbumDataForm;
use Album\Form\AlbumDataFormFactory;
use Album\Model\InputFilter\AlbumInputFilter;
use Interop\Container\ContainerInterface;
use PHPUnit\Framework\TestCase;

class AlbumDataFormFactoryTest extends TestCase
{
    public function testFactory()
    {
        /** @var AlbumInputFilter $inputFilter */
        $inputFilter = $this->prophesize(AlbumInputFilter::class);

        /** @var ContainerInterface $container */
        $container = $this->prophesize(ContainerInterface::class);
        $container->get(AlbumInputFilter::class)
            ->willReturn($inputFilter)
            ->shouldBeCalled();

        $factory = new AlbumDataFormFactory();

        /** @var AlbumDataForm $dataForm */
        $dataForm = $factory($container->reveal());
        $this->assertInstanceof(AlbumDataForm::class, $dataForm);
        $this->assertEquals('text', $dataForm->get('artist')->getAttribute('type'));
        $this->assertEquals('text', $dataForm->get('title')->getAttribute('type'));
        $this->assertEquals('submit', $dataForm->get('save_album')->getAttribute('type'));
    }
}
