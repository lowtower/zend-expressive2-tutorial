<?php

declare(strict_types = 1);

/*
 * Copyright (c) 2017 LowTower
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 *
 * @link https://gitlab.com/lowtower/zend-expressive2-tutorial
 */

namespace AppTest\Http;

use App\Http\AbstractActionFactory;
use AppTest\TestAsset\TestAction;
use AppTest\TestAsset\TestNoConstructorAction;
use Interop\Container\ContainerInterface;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;
use Zend\Expressive\Template\TemplateRendererInterface;

/**
 * Class AbstractActionFactoryTest.
 *
 * @package AppTest
 */
class AbstractActionFactoryTest extends TestCase
{
    /**
     * @var ContainerInterface|ObjectProphecy
     */
    private $container;

    protected function setUp()
    {
        $this->container = $this->prophesize(ContainerInterface::class);
    }

    public function testCanCreateInstanceForAction()
    {
        $factory = new AbstractActionFactory();

        $this->assertTrue($factory->canCreate($this->container->reveal(), 'TestAction'));
    }

    public function testCannotCreateInstanceForInvalidActionClass()
    {
        $factory = new AbstractActionFactory();

        $this->assertFalse($factory->canCreate($this->container->reveal(), 'SomeMiddleware'));
    }

    public function testCreateObjectForTestActionWithFullyQualifiedNameDependency()
    {
        $templateRenderer = $this->prophesize(TemplateRendererInterface::class);
        $this->container
            ->get(TemplateRendererInterface::class)
            ->shouldBeCalled()
            ->willReturn($templateRenderer->reveal());

        $factory = new AbstractActionFactory();
        $object  = $factory($this->container->reveal(), TestAction::class);

        $this->assertInstanceOf(TestAction::class, $object);
    }

    public function testCreateObjectForTestActionWithoutConstructor()
    {
        $factory = new AbstractActionFactory();
        $object  = $factory($this->container->reveal(), TestNoConstructorAction::class);

        $this->assertInstanceOf(TestNoConstructorAction::class, $object);
    }
}
