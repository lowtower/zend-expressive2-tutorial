#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# We can enable extensions with this simple line. We need the ZIP extension to run for Composer
docker-php-ext-enable bcmath mcrypt pdo_mysql intl gd zip bz2 opcache

# If your tests need a special php.ini setting, you can copy you own php.ini file here.
# This is useful for instance to increase the memory limit
# cp ci/php.ini /usr/local/etc/php/php.ini

# Let's run composer install
composer global require 'hirak/prestissimo'
composer install

# Installs Sensiolabs security checker to check against unsecure libraries
php -r "readfile('http://get.sensiolabs.org/security-checker.phar');" > /usr/local/bin/security-checker
chmod +x /usr/local/bin/security-checker

# washingmachine
cd /root
composer create-project thecodingmachine/washingmachine --stability=dev
cd -
composer install
