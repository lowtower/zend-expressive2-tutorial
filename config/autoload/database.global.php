<?php

declare(strict_types=1);

/*
 * Copyright (c) 2017 LowTower
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 *
 * @link https://gitlab.com/lowtower/zend-expressive2-tutorial
 */

return [
    'db' => [
        'driver'  => 'pdo',
        'dsn'     => 'mysql:dbname=album-tutorial;host=localhost;charset=utf8',
        'user'    => 'album',
        'pass'    => 'album',
    ],
];
