<?php

declare(strict_types=1);

/*
 * Copyright (c) 2017 LowTower
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 *
 * @link https://gitlab.com/lowtower/zend-expressive2-tutorial
 */

namespace Album;

use Zend\ServiceManager\AbstractFactory\ConfigAbstractFactory;

/**
 * The configuration provider for the Album module.
 *
 * @see https://docs.zendframework.com/zend-component-installer/
 */
class ConfigProvider
{
    /**
     * Returns the configuration array.
     *
     * To add a bit of a structure, each section is defined in a separate
     * method which returns an array with its configuration.
     *
     * @return array
     */
    public function __invoke()
    {
        return [
            'dependencies'               => $this->getDependencies(),
            'templates'                  => $this->getTemplates(),
            ConfigAbstractFactory::class => $this->getConfigAbstractFactory(),
        ];
    }

    /**
     * Returns the container dependencies.
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            'factories' => [
                Action\AlbumListAction::class         => ConfigAbstractFactory::class,
                Action\AlbumCreateFormAction::class   => ConfigAbstractFactory::class,
                Action\AlbumCreateHandleAction::class => ConfigAbstractFactory::class,
                Action\AlbumUpdateFormAction::class   => ConfigAbstractFactory::class,
                Action\AlbumUpdateHandleAction::class => ConfigAbstractFactory::class,
                Action\AlbumDeleteFormAction::class   => ConfigAbstractFactory::class,
                Action\AlbumDeleteHandleAction::class => ConfigAbstractFactory::class,

                Model\Repository\AlbumRepositoryInterface::class => Model\Repository\AlbumRepositoryFactory::class,

                Model\Storage\AlbumStorageInterface::class => Db\AlbumTableGatewayFactory::class,

                Form\AlbumDataForm::class   => Form\AlbumDataFormFactory::class,
                Form\AlbumDeleteForm::class => Form\AlbumDeleteFormFactory::class,

                Model\InputFilter\AlbumInputFilter::class => Model\InputFilter\AlbumInputFilterFactory::class,
            ],
            'delegators' => [
                \Zend\Expressive\Application::class => [
                    Factory\RoutesDelegator::class,
                ],
            ],
        ];
    }

    /**
     * Returns the container dependencies.
     *
     * @return array
     */
    public function getConfigAbstractFactory()
    {
        return [
            Action\AlbumListAction::class => [
                \Zend\Expressive\Template\TemplateRendererInterface::class,
                Model\Repository\AlbumRepositoryInterface::class,
            ],
            Action\AlbumCreateFormAction::class => [
                \Zend\Expressive\Template\TemplateRendererInterface::class,
                Form\AlbumDataForm::class,
            ],
            Action\AlbumCreateHandleAction::class => [
                \Zend\Expressive\Router\RouterInterface::class,
                Model\Repository\AlbumRepositoryInterface::class,
                Form\AlbumDataForm::class,
            ],
            Action\AlbumDeleteFormAction::class => [
                \Zend\Expressive\Template\TemplateRendererInterface::class,
                Model\Repository\AlbumRepositoryInterface::class,
                Form\AlbumDeleteForm::class,
            ],
            Action\AlbumDeleteHandleAction::class => [
                \Zend\Expressive\Router\RouterInterface::class,
                Model\Repository\AlbumRepositoryInterface::class,
            ],
            Action\AlbumUpdateFormAction::class => [
                \Zend\Expressive\Template\TemplateRendererInterface::class,
                Model\Repository\AlbumRepositoryInterface::class,
                Form\AlbumDataForm::class,
            ],
            Action\AlbumUpdateHandleAction::class => [
                \Zend\Expressive\Router\RouterInterface::class,
                Model\Repository\AlbumRepositoryInterface::class,
                Form\AlbumDataForm::class,
            ],
        ];
    }

    /**
     * Returns the templates configuration.
     *
     * @return array
     */
    public function getTemplates()
    {
        return [
            'paths' => [
                'album' => [__DIR__ . '/../templates/album'],
            ],
        ];
    }
}
