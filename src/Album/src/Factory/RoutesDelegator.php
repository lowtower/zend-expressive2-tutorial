<?php

declare(strict_types = 1);

/*
 * Copyright (c) 2017 LowTower
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 *
 * @link https://gitlab.com/lowtower/zend-expressive2-tutorial
 */

namespace Album\Factory;

use Album\Action;
use Psr\Container\ContainerInterface;
use Zend\Expressive\Application;

class RoutesDelegator
{
    /**
     * @param ContainerInterface $container
     * @param string             $serviceName name of the service being created
     * @param callable           $callback    creates and returns the service
     *
     * @return Application
     */
    public function __invoke(ContainerInterface $container, $serviceName, callable $callback)
    {
        /** @var $app Application */
        $app = $callback();

        // Setup routes:
        $app->get('/album', Action\AlbumListAction::class, 'album');
        $app->get('/album/create', Action\AlbumCreateFormAction::class, 'album-create');
        $app->post('/album/create/handle', [
            Action\AlbumCreateHandleAction::class,
            Action\AlbumCreateFormAction::class,
        ], 'album-create-handle');
        $app->get("/album/update/{id:\d+}", Action\AlbumUpdateFormAction::class, 'album-update');
        $app->post("/album/update/{id:\d+}/handle", [
            Action\AlbumUpdateHandleAction::class,
            Action\AlbumUpdateFormAction::class,
        ], 'album-update-handle');
        $app->get("/album/delete/{id:\d+}", Action\AlbumDeleteFormAction::class, 'album-delete');
        $app->post("/album/delete/{id:\d+}/handle", [
            Action\AlbumDeleteHandleAction::class,
            Action\AlbumDeleteFormAction::class,
        ], 'album-delete-handle');

        return $app;
    }
}
