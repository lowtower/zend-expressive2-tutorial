<?php

declare(strict_types = 1);

/*
 * Copyright (c) 2017 LowTower
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 *
 * @link https://gitlab.com/lowtower/zend-expressive2-tutorial
 */

namespace Album\Model\Repository;

use Album\Model\Entity\AlbumEntity;
use Zend\Paginator\Paginator;

interface AlbumRepositoryInterface
{
    /**
     * Fetch all albums.
     *
     * @param bool $paginated
     *
     * @return AlbumEntity[]|Paginator
     */
    public function fetchAllAlbums($paginated);

    /**
     * Fetch a single album by identifier.
     *
     * @param int $id
     *
     * @return null|AlbumEntity
     */
    public function fetchSingleAlbum($id);

    /**
     * Save an album.
     *
     * Should insert a new album if no identifier is present in the entity, and
     * update an existing album otherwise.
     *
     * @param AlbumEntity $album
     *
     * @return bool
     */
    public function saveAlbum(AlbumEntity $album);

    /**
     * Delete an album.
     *
     * @param AlbumEntity $album
     *
     * @return bool
     */
    public function deleteAlbum(AlbumEntity $album);
}
