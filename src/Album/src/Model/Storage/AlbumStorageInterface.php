<?php

declare(strict_types = 1);

/*
 * Copyright (c) 2017 LowTower
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 *
 * @link https://gitlab.com/lowtower/zend-expressive2-tutorial
 */

namespace Album\Model\Storage;

use Album\Model\Entity\AlbumEntity;
use Zend\Paginator\Paginator;

interface AlbumStorageInterface
{
    /**
     * Fetch a list of albums.
     *
     * @param bool $paginated
     *
     * @return AlbumEntity[]|Paginator
     */
    public function fetchAlbumList($paginated);

    /**
     * Fetch an album by identifer.
     *
     * @param int $id
     *
     * @return null|AlbumEntity
     */
    public function fetchAlbumById($id);

    /**
     * Insert an album into storage.
     *
     * @param AlbumEntity $album
     *
     * @return bool
     */
    public function insertAlbum(AlbumEntity $album);

    /**
     * Update an album.
     *
     * @param AlbumEntity $album
     *
     * @return bool
     */
    public function updateAlbum(AlbumEntity $album);

    /**
     * Delete an album.
     *
     * @param AlbumEntity $album
     *
     * @return bool
     */
    public function deleteAlbum(AlbumEntity $album);
}
